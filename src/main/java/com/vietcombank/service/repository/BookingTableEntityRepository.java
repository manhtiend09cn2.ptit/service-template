package com.vietcombank.service.repository;

import com.vietcombank.service.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BookingTableEntityRepository extends JpaRepository<Booking, UUID> {
}