package com.vietcombank.service.service;

import com.vietcombank.service.entity.Booking;
import com.vietcombank.service.repository.BookingTableEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class BookingService {
    @Autowired
    private BookingTableEntityRepository bookingEntityRepository;

    public List<Booking> retrieveAll() {
        return bookingEntityRepository.findAll();
    }

    public Optional<Booking> retrieveByID(UUID id) {
        return bookingEntityRepository.findById(id);
    }
    public Booking creatNewBooking(Booking booking) {
        booking.setId(UUID.randomUUID());
        booking.setBook_time_create(Instant.now());
       return bookingEntityRepository.save(booking);
    }

    public Booking updateBooking(Booking booking) throws Exception {
        Booking booking1 = bookingEntityRepository.findById(UUID.fromString(String.valueOf(booking.getId()))).get();
        booking1.setBook_time_update(Instant.now());
        booking1.setIs_pair(booking.getIs_pair());
        booking1.setIs_cancel(booking.getIs_cancel());
        booking1.setSchedule_id(booking.getSchedule_id());
        booking1.setCustomer_id(booking.getCustomer_id());
        booking1.setMovie_id(booking.getMovie_id());
        booking1.setNum_of_seat(booking.getNum_of_seat());
        booking1.setTotal_amount(booking.getTotal_amount());
        return bookingEntityRepository.save(booking1);
    }

    public Booking deleteBooking(String booking) {
        Booking booking1 = bookingEntityRepository.getReferenceById(UUID.fromString(booking));
        bookingEntityRepository.delete(booking1);
        return booking1;
    }
}
