package com.vietcombank.service.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tbl_booking")
public class Booking {

    @Id
    @Getter
    @Setter
    @Column(name = "booking_id", nullable = false)
    private UUID id;

    @Getter
    @Setter
    @Column(name = "customer_id")
    private String customer_id;

    @Getter
    @Setter
    @Column(name = "movie_id")
    private String movie_id;

    @Getter
    @Setter
    @Column(name = "schedule_id")
    private String schedule_id;

    @Getter
    @Setter
    @Column(name = "num_of_seat")
    private int num_of_seat;

    @Getter
    @Setter
    @Column(name = "total_amount")
    private int total_amount;

    @Getter
    @Setter
    @Column(name = "is_pair")
    private String is_pair;

    @Getter
    @Setter
    @Column(name = "is_cancel")
    private String is_cancel;

    @Getter
    @Setter
    @Column(name = "book_time_create")
    private Instant book_time_create;

    @Getter
    @Setter
    @Column(name = "book_time_update")
    private Instant book_time_update;

    @Getter
    @Setter
    @Column(name = "status")
    private String status;

}