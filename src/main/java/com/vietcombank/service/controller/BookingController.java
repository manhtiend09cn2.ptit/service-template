package com.vietcombank.service.controller;

import com.vietcombank.service.entity.Booking;
import com.vietcombank.service.service.BookingService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/restapi")
public class BookingController {
    @Autowired
    private BookingService bookingService;

    @GetMapping(value = "/v1/bookings", produces = "application/json")
    @Operation(summary = "Retrieve All Booking")
    public ResponseEntity<?> retrieveMovies() {
        log.info("retrieve all Booking");
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put("data", bookingService.retrieveAll());
        map.put("status", 1);
        map.put("message", "SUCCESS");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping(value = "/v1/bookings/{id}", produces = "application/json" )
    @Operation(summary = "Retrieve Booking by ID")
    public ResponseEntity<?> retrieveBookingsByID(@PathVariable String id) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put("data", bookingService.retrieveByID(UUID.fromString(id)));
        map.put("status", 1);
        map.put("message", "SUCCESS");
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @PostMapping(value = "/v1/create_booking", produces = "application/json")
    @Operation(summary = "Create Booking")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<?> addBookings(@RequestBody Booking booking) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        try{
            Booking bookingCreated = bookingService.creatNewBooking(booking);
            map.put("data",bookingCreated);
            map.put("status", 1);
            map.put("message", "SUCCESS");
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch(Exception e ){
            map.put("data",e.getMessage());
            map.put("status", 100);
            map.put("message", "ERROR");
            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/v1/delete_booking", produces = "application/json")
    @Operation(summary = "Delete Booking")
    public ResponseEntity<?> deleteBookings(@RequestParam String id) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        try{
            Booking bookingDelete = bookingService.deleteBooking(id);
            if(bookingDelete == null){
                map.put("status", 2);
                map.put("message", "NOT FOUND");
                return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
            }
            map.put("data",bookingDelete);
            map.put("status", 1);
            map.put("message", "SUCCESS");
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch(Exception e ){
            map.put("data",e.getMessage());
            map.put("status", 100);
            map.put("message", "ERROR");
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping(value = "/v1/update_booking", produces = "application/json")
    @Operation(summary = "Update Booking")
    @CrossOrigin(origins = "http://localhost:3000")
    public ResponseEntity<?> updateBookings(@RequestBody Booking booking) {
//        bookingService.updateBooking(booking);
//        return new ResponseEntity<>("SUCCESS" , HttpStatus.OK);

        Map<String, Object> map = new LinkedHashMap<String, Object>();
        try{
            Booking bookingUpdate = bookingService.updateBooking(booking);
            if(bookingUpdate == null){
                map.put("status", 2);
                map.put("message", "NOT FOUND");
                return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
            }
            map.put("data",bookingUpdate);
            map.put("status", 1);
            map.put("message", "SUCCESS");
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch(Exception e ){
            map.put("data",e.getMessage());
            map.put("status", 100);
            map.put("message", "ERROR");
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }
}
