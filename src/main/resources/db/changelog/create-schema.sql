--liquibase formatted sql
--changeset microservice_class:20230725_1

CREATE
EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE TBL_BOOKING
(
    BOOKING_ID         uuid PRIMARY KEY default uuid_generate_v4(),
    CUSTOMER_ID    varchar(255),
    MOVIE_ID    varchar(255),
    SCHEDULE_ID    varchar(255),
    TOTAL_AMOUNT    varchar(255),
    NUM_OF_SEAT    numeric,
    IS_PAIR    varchar(255),
    IS_CANCEL    varchar(255),
    STATUS    varchar(255), -- CREATED, PENDING , SUCCESS , FAILED
    BOOK_TIME_CREATE timestamp,
    BOOK_TIME_UPDATE timestamp
);